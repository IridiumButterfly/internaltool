//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// GLOBAL VARIABLES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
var row,
    td,
    name,
    forSale,
    price,
    stock,
    discovery,
    nameInput,
    priceInput,
    stockInput,
    saleInput,
    discoveryInput,
    deleteBtn = $('.btn.delete'),
    editBtn = $('.btn.edit'),
    submitBtn = $('.btn.submit');
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// END VARIABLES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function isChecked($inputElement) {
    console.log($inputElement);
    if($inputElement){
        if($inputElement.target){
            $inputElement = $($inputElement.target);
        }
        if($inputElement.prop('checked') === true) {
            return 'yes';
        } else {
            return 'no';
        }

    } else {
        console.log('$inputElement is an undefined variable');
    }
}

$('#toggle1').on( 'change', isChecked);
$('#toggle2').on( 'change', isChecked);
$('#toggle3').on( 'change', isChecked);
$('#toggle4').on( 'change', isChecked);
$('#addSubmit').on('click', addItem);
$('#editSubmit').on('click', updateItem);
$('.delete').on('click', removeItem);



function addItem(items) {
    event.preventDefault();
    var name = $('.name').val(),
        price = $('.price').val(),
        stock = $('.stock').val(),
        row= $('#inventory tr:last'),
        td = $('#inventory td:last');

    //name
    row.after('<tr><td><div class="textOverFlow"></div><input disabled class="listed" type="text" name="name" value="' + name +
    //price
    '"/></td><td>' + '<span class="poundSign">&pound;</span><input disabled class="listed" type="text" name="price" value="' + price +
    //stock
    '"/></td><td><div class="textOverFlow"></div><input disabled class="listed" type="text" name="stock" value="' + stock +
    //yes/no
    '"/></td><td><input disabled class="listed" type="text" name="sale" value="' + isChecked($('#toggle1')) +
    //show/hide
    '"/></td><td><input disabled class="listed" type="text" name="discovery" value="' + isChecked($('#toggle2')) +
    //delete
    '"/></td><td>' + '<button class="delete btn"></button>' +
    //edit
    '</td><td>' + '<button class="edit btn"></button>' +
    //close <tr>
    '</td><td></tr>' );

    $('.btn.delete').off('click');
    $('.btn.delete').on("click", removeItem);
    $('.btn.edit').off('click');
    $('.btn.edit').on("click", editItem);
    $('#updatedItem').removeClass('hide').addClass('show');
    $('.return').on('click', resetModals);
    clearInputs();
}

function removeItem(event) {
    event.preventDefault();
    var  confirmDelete = confirm("Are you sure you want to delete this item?");
    if (confirmDelete == true) {
        var parentRow = $(this).parent().parent();
        parentRow.remove();
    } else {
        return false;
    }
}

function updateItem(event) {
    var editInputsArr = $('#editInventoryItem').find('input');
    event.preventDefault();
    console.log(editInputsArr);
    $.each(editInputsArr, function(index, inputElement) {
        console.log(inputElement);
        switch ($(editInputsArr[index]).attr('name')) {
            case 'name':
                nameInput.val($(inputElement).val());
                break;
            case 'price':
                priceInput.val($(inputElement).val());
                break;
            case 'stock':
                stockInput.val($(inputElement).val());
                break;
            case 'sale':
                saleInput.val(isChecked($(inputElement)));
                break;
            case 'discovery':
                discoveryInput.val(isChecked($(inputElement)));
                break;
            default: throw new Error('updateItem() failed to identify the input element\'s name attribute within form.js');
        }
    });
    $('#updatedItem').removeClass('hide').addClass('show');
}

function addValToEditInputs(items) {
    for( key in items) {
        var currentInput = $('#editInventoryItem').find('input[name = "' + key + '" ]');

        if(currentInput.attr('type') === 'checkbox') {
        console.log('We have checkboxes!!');
            if(items[key] === "yes") {
                console.log('Sale is YES!', currentInput);
                currentInput.attr('checked', true);

            } else {
                console.log('Sale is NO!');
                currentInput.attr('checked', false);
            }

        } else {
            currentInput.val(items[key]);
        }
    }
}

function editItem(event) {
    event.preventDefault();
        $('#editInventoryItem').removeClass('hide').addClass('show');
        var parentTRow = $(this).parent().parent(),
            itemArr = parentTRow.find('input'),
            items = {
                name: '',
                price: '',
                stock: '',
                sale: '',
                discovery: ''
            };

        $.each(itemArr, function(index, inputElement) {
            var nameAttr = $(this).attr('name'),
                valAttr = $(this).attr('value');
            switch(nameAttr) {
                case 'name':
                    nameInput = $(this);
                    break;
                case 'price':
                    priceInput = $(this);
                    break;
                case 'stock':
                    stockInput = $(this);
                    break;
                case 'sale':
                    saleInput = $(this);
                    break;
                case 'discovery':
                    discoveryInput = $(this);
                    break;
                default:
                    throw new Error('editItem() failed to identify the input element\'s name attribute within form.js');
            }
            items[nameAttr] = valAttr;
        });

        addValToEditInputs(items, nameInput, priceInput, stockInput, saleInput, discoveryInput);
        $('.return').on('click', resetModals);
}

//CLEAR INPUTS
function clearInputs(){
        $('#addInv input').val('').delay('fast');
        $('#addInv input').removeAttr('checked').delay('fast');
        $('#manageInv input').val('').delay('fast');
        $('#manageInv input').removeAttr('checked').delay('fast');
}

//RESET ALL MODALS
function resetModals(event) {
    $('#editInventoryItem').removeClass('show').addClass('hide');
    $('#updatedItem').removeClass('show').addClass('hide');
}
